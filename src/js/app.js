import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  document.body.addEventListener("click", () => {
    const main = document.querySelector(".main");
    const items = [
      `Hello, it's me`,
      `I was wondering if after all these years you'd like to meet`,
      `To go over everything`,
      `They say that time's supposed to heal ya`,
      `But I ain't done much healing`,
    ];

    items.forEach((item) => {
      const article = document.createElement("article");
      article.classList.add("message", "is-primary");

      const body = document.createElement("div");
      body.classList.add("message-body");

      body.innerText = item;

      article.appendChild(body);
      main.appendChild(article);
    });
  });
});
